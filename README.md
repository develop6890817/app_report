# app_report



## Inicio del proyecto

Para este reto se dividio el desarrollo de backend y frotend con el objetivo de aprovechar el paradigma de los microservicios.

## Microservicio

desarrollo de software que estructura una aplicación como un conjunto de pequeños servicios independientes, cada uno de los cuales se centra en una tarea o función específica y se ejecuta de forma autónoma. Estos servicios están diseñados para ser altamente cohesivos, débilmente acoplados y pueden ser desarrollados, implementados, y escalados de forma independiente.

## Ventajas

- [ ] [escabalidad]
- [ ] [independencia]
- [ ] [divide y venceras]
- [ ] [comunicacion de apis]

## Enlaces de los repositorios

- [ ] [backend](https://gitlab.com/develop6890817/web_app_backend)
- [ ] [frontend](https://gitlab.com/develop6890817/web_app_frotend)


## Autor
Jose Manolo Pinzon Hernandez

